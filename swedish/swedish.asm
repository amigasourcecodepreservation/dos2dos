		Dos-2-Dos text messages.


'Enter the device to be used for MS-DOS/ATARI files: '
'Ange enhet att anv�ndas f�r MS-DOS/ATARI-filer: '

'Your Amiga auto-configuration equipment list doesn''t include DFn.'
'Din Amigas autokonfigurerade tillbeh�rslista inkluderar ej DFn.

'Dos-2-Dos assumes it is a non-Amiga external 5.25-inch drive.'
'Dos-2-Dos antar att det �r en icke-Amiga 5.25" diskettenhet.'

'D2D drive: command'
'D2D diskettenhet: kommando'

'Example: D2D DF2: COPY DF2:*.BAT -A -R'
'Exempel: D2D DF2: COPY DF2:*.BAT -A -R'

'Out of memory.'
'Ej nog minne.'

'Bad arguments.'
'Fel argument.'

'Source and destination drives cannot be the same.'
'Utg�angs och destinationsenhet kan inte vara samma.'

'Destination file already exists.  Replace it? '
'Destinationsfilen existerar redan. Ers�tta den? '

'File "filename" already exists.  Replace it? '
'Filen "filnamn" existerar redan. Ers�tta den? '

'Can't find that file.'
'Kan ej hitta den filen.'

'Invalid MS-DOS file name.'
'Otill�tet MS-DOS filnamn.'

'Device not defined in Mountlist.'
'Enheten ej definierad i Mountlist.'

'Unable to locate that device or file.'
'Kan ej hitta den enheten eller filen.'

'Unable to create AmigaDOS file.'
'Kan ej skapa AmigaDos-filen.'

'Unknown command.  Type HELP for command summary.'
'Ok�nt kommando. Skriv "?" f�r summering av kommandona.'

'Please enter a new MS-DOS file name: '
'Ange ett nytt MS-DOS filnamn.'

'Please answer "yes" or "no".'
'Ange "ya" eller "nej".'

'Type HELP or ? for summary of Dos-2-Dos commands.'
'Skriv "?" f�r summering av Dos-2-Dos kommandona.'

'Warning -- you cannot use DF0 to access AmigaDOS files.'
'Varning -- du kan inte anv�nda DF0 f�r AmigaDOS-filer.'

'Use RAM: or an external floppy drive.  See Readme notes.'
'Anv�nd RAM: eller an extern diskettenhet.'

'Press Return for more, press ESC Return to exit.'
'Tryck p� RETUR f�r mer eller ESC f�r att avsluta.'

'End of file.  Press Return.'
'Slut p� filen. Tryck RETUR.'

'Insert diskette into drive and press RETURN when ready.'
'Mata in disketten i diskettenheten och tryck d�refter RETUR.'

'Write error; formatting failed.'
'Skrivfel: formatteringen misslyckades.'

'Formatting complete.'
'Formatteringen komplett.'

'  Copying <filename>'
'  Kopeirar <filnamn>'

'***Break'
'***Avbrott'

'  Deleting <filename>'
'  Raderar <filnamn>'

'MS-DOS/Atari disk write error.'
'Skrivfel p� MS-DOS-disketten.'

'MS-DOS/Atari drive not ready.'
'MS-DOS-diskettenheten ej klar.'

'MS-DOS/Atari drive read error.'
'L�sfel p� MS-DOS-disketten.'

'MS-DOS/Atari disk is write protected.'
'MS-DOS-disketten �r skrivskyddad.'

'Can't open timer device.'
'Kan ej �ppna "timer device".'

'Can't delete that file.'
'Kan ej radera den filen.'

'Invalid directory.'
'Otill�ten katalog.'

'MS-DOS/Atari disk full.'
'MS-DOS-disketten full.

'MS-DOS/Atari directory full.'
'MS-DOS-katalogen full.'

'Directory of <volume> '
'Katalog �ver <volym> '

'  <DIR> '
'  <KAT> '

' nnnn file(s),   '
' nnnn fil(er),   '

' nnnn bytes free'
' nnnn bytes lediga'

'Can't format - not a floppy disk.'
'Kan ej formattera - ej en diskett.'

'Not valid MS-DOS/Atari format.'
'Ej korrekt MS-DOS diskettformat.'

'Formatting track nn.'
'Formatterar sp�r nn.'


The following text is the Dos-2-Dos command summary which is printed in
response to a '?' or the HELP command.  The description words were carefully 
chosen to fit one line with enough space for the example on the same line:

'        Dos-2-Dos Command Summary:'
'        Dos-2-Dos Kommandosummering:'

'  Display directory.............. DIR or DIR drive:path'
'  Visa katalog................... DIR eller DIR enhet:s�kv�g'

'  Change current directory....... CHDIR drive:path'
'  �ndra aktuell katalog.......... CHDIR enhet:s�kv�g

'  Display ASCII file contents.... TYPE drive:path\file'
'  Visa ASCII-fil................. TYPE enhet:s�kv�g\fil'

'  Copy a file (general form)..... COPY drive:path\file drive:path/file -A -R'
'  Kopiera en fil (allm�n form)... COPY enhet:s�kv�g\fil enhet:s�kv�g/fil -A -R'

'  Copy one ASCII file............ COPY DF2:MYFILE.ASM DF0:C/NEWFILE.ASM -A'
'  Kopiera en ASCII-fil........... COPY DF2:MINFIL.ASM DF0:C/NYFIL.ASM -A

'  Copy MS-DOS/Atari files........ COPY DF2:*.ASM'
'  Kopiera MS-DOS/ATARI-filer..... COPY DF2:*.ASM'

'  Copy AmigaDOS files............ COPY #?.ASM'
'  Kopiera AmigaDOS-filer......... COPY DF2:#?.ASM'

'  Copy and convert ASCII files... add -A to COPY command line'
'  Kopiera/konvertera ASCII-filer. l�gg till -A p� COPY-raden'

'  Suppress 'Replace?' question... add -R to COPY command line'
'  Bekr�fta ej vid "Ers�tta?"..... l�gg till -R p� COPY-raden'

'  Delete a file.................. DELETE drive:path\file'
'  Radera en fil.................. DELETE enhet:s�kv�g\fil

'  Format an MS-DOS DSDD disk..... FORMAT'
'  Formattera DSDD MS-DOSdiskett.. FORMAT'

'  Format an Atari ST DSDD disk... FORMAT /A'
'  Formattera DSDD AtariSTdiskett. FORMAT /A' 

'  Display this summary........... HELP, or ?'
'  Visa denna summering........... ?'

'  Select another MS-DOS drive.... RESTART'
'  Byt till en annan MS-DOS enhet. RESTART'

'  Exit to AmigaDOS............... EXIT, or X'
'  Avsluta programmet............. EXIT, eller X'

